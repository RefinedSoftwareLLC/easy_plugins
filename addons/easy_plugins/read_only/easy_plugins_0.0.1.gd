@tool
class_name Easy extends Node
## Shared Library from EasyPlugins.

################################################################################
# await
signal quit_requested
signal process_started
signal physics_started
signal draw_starts_next
signal draw_ended

static func forward(sourceSignal: Signal, targetSignal: Signal):
	var c = Easy.Callable(targetSignal) # or targetSignal if fixed
	if not sourceSignal.is_connected(c):
		sourceSignal.connect(c) #May need CONNECT_REFERENCE_COUNTED

static func forwardEmit(a=null, b=null, c=null, d=null, e=null, f=null, g=null, h=null, i=null, j=null, k=null, l=null, m=null, n=null, o=null, p=null, q=null, r=null, s=null, t=null, u=null, v=null, w=null, x=null, y=null, z=null) -> void:
	var args = [null, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z]
	for arg_i in range(args.size() - 1, 0, -1):
		if args[arg_i] is Signal:
			args[0] = args[arg_i].get_name()
			var obj = args[arg_i].get_object()
			args.resize(arg_i)
			obj.callv("emit_signal", args)
			return
	print("EasyPlugins could not find Signal to emit from")
	
static func forwardWhat(reversed, object, what) -> void:
#	reversed: bool = false, object, what: int
	if what == null:
		what = object
		object = reversed
		reversed = false
	if reversed:
		object.notification(what, true)
	else:
		object.notification(what)

static func Callable(object, method_or_what: Variant = null) -> Callable:
	if method_or_what == null: # one argument
		if object is Callable:
			return Callable(object)
		if object is Signal:
			return Callable(Easy, "forwardEmit").bind(object) # ugh, it adds after instead of before.
		return Callable()
	elif method_or_what is int: # 2nd arg is what
		return Callable(Easy, "forwardWhat").bind(object, method_or_what)
	else: # 2nd arg is method
		if object.has_method(method_or_what):
			return Callable(object, method_or_what)
		if object.has_signal(method_or_what):
			return Callable(Easy, "forwardEmit").bind(object[method_or_what]) # ugh, it adds after instead of before.
		return Callable()

################################################################################
# Common
static func quit_request(tree: SceneTree):
	tree.notification(MainLoop.NOTIFICATION_WM_QUIT_REQUEST)

static func tree():
#	return (Engine.get_main_loop() as SceneTree).current_scene.get_tree()
	return (Engine.get_main_loop() as SceneTree).root.get_tree()

static func get_script_native_type(script) -> String:
	if script.has_method("get_script"):
		script = script.get_script()
	return script.get_instance_base_type() as String

static func get_script_extends_type(script) -> String:
	if script.has_method("get_script"):
		script = script.get_script()
	return find_var_or_path(script.source_code, "extends")

static func get_script_type(script) -> String:
	if script.has_method("get_script"):
		script = script.get_script()
	return find_var(script.source_code, "class_name")

static func preload_directory():
#	Manually Generate a dirname.import.gd full of: preload(filename)
	pass
################################################################################
# Advanced

static func find_var(source: String, name: String, default: String = "") -> String:
	var ex = RegEx.new()
	#	https://www.pcre.org/current/doc/html/pcre2pattern.html
	ex.compile("(?<=" + name + ")\\s+[a-zA-Z][a-zA-Z0-9]+")
	var found = ex.search(source)
	if not found:
		push_error(name + " not found in file"); assert(found); return default
	found = found.get_string().strip_edges()
	if found.begins_with("\""): found = found.substr(1, found.length() - 2)
	return found

static func find_var_or_path(source: String, name: String, default: String = "") -> String:
	var ex = RegEx.new()
	#	https://www.pcre.org/current/doc/html/pcre2pattern.html
	ex.compile("(?<=" + name + ")\\s+(\"res://[a-zA-Z0-9/._]+.gd\"|[a-zA-Z][a-zA-Z0-9]+)")
	var found = ex.search(source)
	if not found:
		push_error(name + " not found in file"); assert(found); return default
	found = found.get_string().strip_edges()
	if found.begins_with("\""): found = found.substr(1, found.length() - 2)
	return found

################################################################################
# Internals

func _init():
	print("Easy ready")
	print(Engine.get_main_loop().get_signal_list())
	print(Engine.get_main_loop().get_property_list())
	var tree = Easy.tree()
	print("tree", tree)
	tree.set_auto_accept_quit(false)
	forward(tree.process_frame, self.process_started)
	forward(tree.physics_frame, self.physics_started)
	forward(tree.frame_pre_draw, self.draw_starts_next)
	forward(tree.frame_post_draw, self.draw_ended)

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_CLOSE_REQUEST:
		emit_signal("quit_requested")
