@tool
class_name EasyPluginsManager extends EditorPlugin

#@onready var Easy = load("easy_plugins_0.0.1.gd").get_class()

signal started
signal ended
func _started():
	print("started")
func _ended():
	print("ended")
func forwardEmit(a=null, b=null, c=null, d=null, e=null, f=null, g=null, h=null, i=null, j=null, k=null, l=null, m=null, n=null, o=null, p=null, q=null, r=null, s=null, t=null, u=null, v=null, w=null, x=null, y=null, z=null) -> void:
	var args = [null, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z]
	for arg_i in range(args.size() - 1, 0, -1):
		if args[arg_i] is Signal:
			args[0] = args[arg_i].get_name()
			var obj = args[arg_i].get_object()
			args.resize(arg_i)
			obj.callv("emit_signal", args)
			return
	print("EasyPlugins could not find Signal to emit from")

func _enter_tree():
	self.started.connect(Callable(self, "_started"))
	self.ended.connect(Callable(self, "_ended"))
#	self.started.connect(Callable(self.ended as Object, "emit"))
	var c = Callable(self, "forwardEmit").bind(self.ended)
#	print("[[")
#	c.call()
#	print("]]")
	self.started.connect(c)
	self.started.emit()
	print("done")
	pass

#	var easyPath = self.get_script().resource_path.get_base_dir() + "/easy_plugins_0.0.1.gd"
#	print("folder: ", easyPath)
#	print("Easy: ", Easy)
#	print("EasyNode: ", EasyNode)
	
#"easy_plugins_0.0.1.gd")
#	if not File.file_exists("res://read_only/easy_plugins_0.0.1.gd"):
#		Directory.copy("read_only/easy_plugins_0.0.1.gd", "res://read_only/easy_plugins_0.0.1.gd")
	
#	await self.get_tree().process_frame
#
#	var list = Engine.get_singleton_list()
##	print(list) # [Performance, TextServerManager, ProjectSettings, IP, Geometry2D, Geometry3D, ResourceLoader, ResourceSaver, OS, Engine, ClassDB, Marshalls, TranslationServer, Input, InputMap, EngineDebugger, Time, NativeExtensionManager, ResourceUID, JavaClassWrapper, JavaScript, NavigationMeshGenerator, VisualScriptCustomNodes, DisplayServer, RenderingServer, AudioServer, PhysicsServer2D, PhysicsServer3D, NavigationServer2D, NavigationServer3D, XRServer, CameraServer, GDScriptLanguageProtocol]
#	print(Easy.get_native_type(self))

func _exit_tree():
	# Clean-up of the plugin goes here.
	pass

func _init():
	print("EasyPluginsManager ready")
